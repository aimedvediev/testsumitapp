
import React, { Component } from 'react';
import { TextInput, StyleSheet, Text, View, TouchableHighlight, Keyboard, Button } from 'react-native';
import MapView, { Polyline, Marker } from "react-native-maps";
//import apiKey from "./google_api_key";
import _ from "lodash";
import PolyLine from "@mapbox/polyline";
export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0,
      error: "",
      destination: "",
      startPoint: "",
      predictions: [],
      predictionsToDestination: [],
      predictionsOnStart: [],
      pointCoords: [],
      destinationPlaceId: "Home",
      startPointPlaceId: ""
    };
    this.onChangeDestinationDebounced = _.debounce(
      this.onChangeDestination,
      1000
    );
    this.onChangeStartPointDebounced = _.debounce(
      this.onChangeStartPoint,
      1000
    );
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
      },
      error => console.error(error),
      { enableHighAccuracy: true, timeout: 2000, maximumAge: 20000 }
    );
  }

  async getRouteDirections() {
    try {
      const response = await fetch(
        `https://maps.googleapis.com/maps/api/directions/json?origin=place_id:${this.state.startPointPlaceId}&destination=place_id:${this.state.destinationPlaceId}&key=AIzaSyDBC1QIo7GhF4v0WDqdlQbac0x2kMq-ECk`
      );
      const json = await response.json();
      const points = PolyLine.decode(json.routes[0].overview_polyline.points);
      const pointCoords = points.map(point => {
        return { latitude: point[0], longitude: point[1] };
      });
      this.setState({ pointCoords });
      Keyboard.dismiss();
      this.map.fitToCoordinates(pointCoords);
    } catch (error) {
      console.error(error);
    }
  }

  async onChangeDestination(destination) {
    const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyDBC1QIo7GhF4v0WDqdlQbac0x2kMq-ECk&input=${destination}&location=${this.state.latitude}, ${this.state.longitude}&radius=2000`;
    try {
      const result = await fetch(apiUrl);
      const json = await result.json();
      this.setState({
        predictionsToDestination: json.predictions
      });
    } catch (err) {
      console.error(err);
    }
  }

  async onChangeStartPoint(startPoint) {
    const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyDBC1QIo7GhF4v0WDqdlQbac0x2kMq-ECk&input=${startPoint}&location=${this.state.latitude}, ${this.state.longitude}&radius=2000`;
    try {
      const result = await fetch(apiUrl);
      const json = await result.json();
      this.setState({
        predictionsOnStart: json.predictions
      });
    } catch (err) {
      console.error(err);
    }
  }

  render() {
    let marker = null;
    if (this.state.pointCoords.length > 1) {
      marker = (
        <Marker coordinate={this.state.pointCoords[this.state.pointCoords.length - 1]} />
      );
    }

    const predictionsToDestination = this.state.predictionsToDestination.map(prediction => (
      <TouchableHighlight

        onPress={() => {
          this.setState({ destinationPlaceId: prediction.place_id, predictionsToDestination: [], destination: prediction.structured_formatting.main_text });
        }}
        key={prediction.id}
      >
        <View>
          <Text style={styles.suggestions}>{prediction.structured_formatting.main_text}</Text>
        </View>
      </TouchableHighlight>
    ));
    const predictionsOnStart = this.state.predictionsOnStart.map(prediction => (
      <TouchableHighlight

        onPress={() => {
          this.setState({ startPointPlaceId: prediction.place_id, predictionsOnStart: [], startPoint: prediction.structured_formatting.main_text });
        }}
        key={prediction.id}
      >
        <View>
          <Text style={styles.suggestions}>{prediction.structured_formatting.main_text}</Text>
        </View>
      </TouchableHighlight>
    ));
    return (
      <View style={styles.container}>
        <MapView
          ref={map => {
            this.map = map
          }}
          style={styles.map}
          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
          }}
          showsUserLocation={true}
        >
          <Polyline
            coordinates={this.state.pointCoords}
            strokeWidth={4}
            strokeColor="red"
          />
          {marker}
        </MapView>
        <TextInput
          placeholder="Enter start point..."
          style={styles.startPointInput}
          value={this.state.startPoint}
          clearButtonMode="always"
          onChangeText={
            startPoint => {
              this.setState({ startPoint });
              this.onChangeStartPointDebounced(startPoint);
            }
          }
        />
        {predictionsOnStart}
        <TextInput
          placeholder="Enter destination..."
          style={styles.destinationInput}
          value={this.state.destination}
          clearButtonMode="always"
          onChangeText={destination => {
            this.setState({ destination });
            this.onChangeDestinationDebounced(destination);
          }
          }
        />
        {predictionsToDestination}
        <Button
          onPress={()=>this.getRouteDirections()}
          title="GO"
          color="black"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  suggestions: {
    backgroundColor: "white",
    padding: 5,
    fontSize: 18,
    borderWidth: 0.5,
    marginLeft: 5,
    marginRight: 5
  },
  destinationInput: {
    height: 40,
    borderWidth: 0.5,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    padding: 5,
    backgroundColor: "white"
  },
  startPointInput: {
    height: 40,
    borderWidth: 0.5,
    marginTop: 50,
    marginLeft: 5,
    marginRight: 5,
    padding: 5,
    backgroundColor: "white"
  },
  container: {
    ...StyleSheet.absoluteFillObject
  },
  map: {
    ...StyleSheet.absoluteFillObject
  }
});
